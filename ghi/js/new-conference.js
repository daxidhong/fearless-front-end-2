// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/locations/'

//     const response = await fetch(url);

//     if (response.ok) {
//         const locationData = await response.json();

//         const selectLocation = document.querySelector(".form-select");
//         const locations = locationData.locations;

//         for (let location of locations) {
//           const locationOption = document.createElement("option");
//           locationOption.value = location.id;
//           locationOption.innerHTML = location.name;
//           selectLocation.appendChild(locationOption);
//         }


//     const formTag = document.getElementById('create-conference-form')
//     formTag.addEventListener("submit", async event =>{
//         event.preventDefault();
//         const formData = new FormData(formTag);
//         const json = JSON.stringify(Object.fromEntries(formData));

//         const conferencesUrl = 'http://localhost:8000/api/conferences/';
//         const fetchConfig = {
//             method: "post",
//             body: json,
//             headers: {
//                 'Content-Type': 'application/json',
//             },
//         };

//         const response = await fetch(conferencesUrl, fetchConfig);
//         if (response.ok){
//             formTag.reset();
//             const newConference = await response.json();
//             console.log(newConference)

//         }});
//     }
// });

window.addEventListener("DOMContentLoaded", async () => {
    const locationUrl = "http://localhost:8000/api/locations/";
    const locationResponse = await fetch(locationUrl);

    if (locationResponse.ok) {
      const locationData = await locationResponse.json();

      const selectLocation = document.querySelector(".form-select");
      const locations = locationData.locations;

      for (let location of locations) {
        const locationOption = document.createElement("option");
        locationOption.value = location.id;
        locationOption.innerHTML = location.name;
        selectLocation.appendChild(locationOption);
      }
    }

    const conferenceForm = document.getElementById("create-conference-form");
    conferenceForm.addEventListener("submit", async (event) => {
      event.preventDefault();

      const formData = new FormData(conferenceForm);
      const json = JSON.stringify(Object.fromEntries(formData));

      const conferenceUrl = "http://localhost:8000/api/conferences/";
      const fetchConfig = {
        method: "post",
        body: json,
        headers: { "Content-Type": "application/json" },
      };

      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        conferenceForm.reset();
        const newConference = await response.json();
        console.log(newConference);
      }});
  });
