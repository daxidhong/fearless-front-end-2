// function App(props) {
//   if (props.attendees == undefined) {
//     return null;
//   }
//   return (
//     <div>
//       Number of attendees: {props.attendees.length}
//     </div>
//   );
// }

// export default App;


// // we did this on friday before break 4/7
// import Nav from './Nav';
// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <>
//     <Nav />
//     <div className="container">
//       <table className="table table-striped">
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>
//           {props.attendees.map(attendee => {
//             return (
//               <tr key={attendee.href}>
//                 <td>{ attendee.name }</td>
//                 <td>{ attendee.conference }</td>
//               </tr>
//             );
//           })}
//         </tbody>
//       </table>
//     </div>
//     </>
//   );
// }
// export default App;


// this is our new code 4/17, week 10 d1
// import Nav from './Nav';
// import AttendeesList from './AttendeesList';
// import LocationForm from './LocationForm';
// import ConferenceForm from './ConferenceForm';
// import AttendConferenceForm from './AttendConferenceForm';

// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <>
//       <Nav />
//       <div className="container">
//         {/* <ConferenceForm/> */}
//         {/* <LocationForm/> */}
//         {/* <AttendeesList attendees={props.attendees} /> */}
//         <AttendConferenceForm />
//       </div>
//     </>
//   );
// }

// export default App;


// this is our new code 4/18 week 10 d2
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendconference">
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="attendeeslist">
            <Route path="list" element={<AttendeesList attendees={props.attendees} />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
